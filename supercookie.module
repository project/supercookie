<?php
/**
 * @file
 * Code for the supercookie module.
 */

/**
 * Implements hook_cron().
 */
function supercookie_cron() {

  $config = \Drupal::config('supercookie.settings');
  $database = \Drupal::database();
  $logger = \Drupal::logger('supercookie');
  $word_formatter = \Drupal::translation();

  // Clean up expired/abandoned records.
  $count = 0;
  $interval = $config->get('supercookie_expire');
  if ($interval !== 'calendar_day') {
    $count = $database
      ->delete('supercookie')
      ->where("(created + :interval) > expires", array(
        ':interval' => $interval,
      ))
      ->execute();
  }
  else {
    $count = $database
      ->delete('supercookie')
      ->where("expires < UNIX_TIMESTAMP(DATE_FORMAT(FROM_UNIXTIME(created) + INTERVAL 1 DAY, :date_format))", array(
        ':date_format' => '%Y-%m-%d',
      ))
      ->execute();
  }

  if (!empty($count)) {
    $logger->info('Pruned @count from supercookie.', array(
      '@count' => $count . ' fingerprint ' . $word_formatter->formatPlural($count, 'record', 'records')->render(),
    ));
  }
}
/**
 * Implements hook_page_attachments().
 */
function supercookie_page_attachments(array &$page) {

  // Make sure drupal_check_module is defined and check library requirements.
  // @see https://www.drupal.org/node/2258019
  include_once DRUPAL_ROOT . '/core/includes/install.inc';
  $requirements = drupal_check_module('supercookie');

  // Don't init if we're on admin pages or requirements not met.
  $is_admin = \Drupal::service('router.admin_context')->isAdminRoute();
  if ($is_admin || !$requirements) {
    return;
  }

  $data = \Drupal::service('supercookie.response')
    ->getResponse()
    ->getContent();

  $data = (array) json_decode($data);

  if (!$data['dnt']) {
    $page['#attached']['library'][] = 'supercookie/supercookie';
    $page['#attached']['drupalSettings']['supercookie'] = $data;
  }
}
